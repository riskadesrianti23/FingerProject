@echo "download file untuk call api firebase global.js"
wget -c https://gitlab.com/riskadesrianti23/FingerPrint/raw/master/FingerProject/global.js
@echo "membuat folder src untuk menyimpan file finger.js"
mkdir src
cd src
@echo "download file finger.js"
wget -c https://gitlab.com/riskadesrianti23/FingerPrint/raw/master/FingerProject/src/finger.js
cd ..
@echo "membuat parameter 1 input write file : finger.js untuk membaca file"
SET arg1=%cd%\src\finger.js
SET fileFinger=%arg1:\=\\%
@echo %fileFinger%
@echo "membuat parameter 2 input write file : App.js untuk rewrite file"
SET arg1=%cd%\App.js
SET fileApp=%arg1:\=\\%
@echo %fileApp%
@echo "write file App.js"
java -cp writeFile.jar telkom.dev.main.Main %fileFinger% %fileApp%
@echo "membuat folder assets untuk menyimpan file gambar"
mkdir assets
cd assets
@echo "membuat folder assets untuk menyimpan file gambar"
mkdir images
cd images
@echo "download file gambar untuk fingerprint"
wget -c https://gitlab.com/riskadesrianti23/FingerPrint/raw/master/FingerProject/assets/images/fingerprint.png
cd ../..
@echo "menghubungkan dependency dengan android"
react-native link
