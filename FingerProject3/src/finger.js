import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Image, TouchableOpacity, ImageBackground, TextInput, AsyncStorage, Label, Alert, ActivityIndicator } from 'react-native';
import { Container, Content } from 'native-base';
import { Header, Icon } from 'react-native-elements';
import ResponsiveImage from 'react-native-responsive-image';
//import { AppHeader, AppFooter } from "../app-nav/index"
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import Fingerprint from 'react-native-fingerprint-android';
const GLOBAL = require('../global');
import LinearGradient from 'react-native-linear-gradient';



export default class Finger extends React.Component {
    constructor(props) {
        super(props);
         this.state = {
            email:"",
            password : "",
            isLoading : false,
            message : "Fingerprint",
            datetime : "",
            isModalVisible: false,
            fingerprint: "off"
        }
    }

     async authenticate() {
        this.setState({
            phase: 'normal', 
            message: 'Fingerprint',
        })
        
        try {
            const hardware = await Fingerprint.isHardwareDetected();
            const permission = await Fingerprint.hasPermission();
            const enrolled = await Fingerprint.hasEnrolledFingerprints();

            if(!hardware){
                //this.props.navigation.navigate("Login")
                AsyncStorage.setItem("fingerprintHardware",false)
            } else {
                AsyncStorage.setItem("fingerprintHardware",true)
            }

            if (!hardware || !permission || !enrolled) {
                let message = !enrolled ? 'No fingerprints registered.' : !hardware ? 'This device doesn\'t support fingerprint scanning.' : 'App has no permission.'
                this.setState({
                    phase: 'fail',
                    message : message
                });
                return;
            }

           
                           

            
            await Fingerprint.authenticate(warning => {
                this.setState({
                    phase: 'warn',
                    message: warning.message
                })
            });

            // if we got this far, it means the authentication succeeded.
            this.setState({
                phase: 'success',
                message: ''
            });

            
          
            
        } catch (error) {
            if(error.code == Fingerprint.FINGERPRINT_ERROR_CANCELED) {
                // we don't show this error to the user.
                // we will check if the auth was cancelled & restart the flow when the appstate becomes active again.
                return;
            }
            this.setState({
                phase: 'fail',
                message: error.message
            })
        }
    }
    componentWillMount(){
        // AsyncStorage.getItem("fingerprint", (error, result) => {
        //    if(result!="on"){
             
        //          //this.props.navigation.navigate("Login")
        //    } else {
        //           this.authenticate(); 
        //    }
        // })
            }
    componentDidMount(){
        this.authenticate();

    }

    async componentWillUnmount() {
        try {
            if(!Fingerprint.isAuthenticationCanceled()) {
                //stop listening to authentication.
                await Fingerprint.cancelAuthentication();
            }
        } catch(z) {
            console.error(z);
        }
    }


    render() {
        var a = new Date();
        var d = a.getDate();
        var c = a.getMonth() + 1;
        var b = a.getFullYear();
        var e = a.getHours();
        var f = a.getMinutes();
        var g = a.getSeconds();
        this.state.datetime = b + "-" + c + "-" + d + " " + e + ":" + f + ":" + g;

        return (
            <Container style={{backgroundColor:"white"}}>
               
                    <View style={{backgroundColor:"#f2f2f2", width:"90%",marginLeft:"5%", height:"48%", borderRadius:5, marginTop:"85%", alignItems:"center"}}>
                        {/*<Text>
                            {this.state.phase === 'normal' && 'Touch the fingerprint sensor.'}
                            {this.state.phase === 'warn' && 'Try again.'}
                            {this.state.phase === 'fail' && 'Authentication failed.'}
                            {this.state.phase === 'success' && 'Authentication succeeded.'}
                            
                       </Text>*/}
                       {
                           this.state.phase=="success"&&this.state.isLoading==false ?
                            Alert.alert(
                                'Alert',
                                'Login Sukses',
                                [
                                    { text: 'OK', onPress:this.login}
                                ],
                                { cancelable: false }
                            ) : null
                       }

                        <Text style={{fontFamily:"Raleway-Bold", color:"black", fontSize:responsiveFontSize(2), marginTop:"7%"}}>Login fingerprint</Text>
                        <Text style={{fontFamily:"Raleway-Regular", color:"black", fontSize:responsiveFontSize(2), marginTop:"2%"}}>Letakan sidik jari pada sensor</Text>
                        <ResponsiveImage style={{marginTop:"10%"}} initWidth="70" initHeight="70" resizeMode="contain" source={require('../assets/images/fingerprint.png')} />
                        <Text style={{fontFamily:"Raleway-Regular", color:"black", fontSize:responsiveFontSize(2), marginTop:"2%"}}>{this.state.message}</Text>
                        <View style={{height:"100%", width:"100%",flexDirection:"row", marginTop:"-5%"}}>
                            <TouchableOpacity style={{width:"49.75%",height:"24%",marginTop:"10%",bottom:0, backgroundColor:"#e0e0e0", borderBottomLeftRadius:5, alignItems:"center", justifyContent:"center"}}>
                                <Text style={{fontFamily:"Raleway-Regular", color:"black", fontSize:responsiveFontSize(1.7)}}>Batal</Text>
                            </TouchableOpacity>
                            <TouchableOpacity //onPress={() => {Fingerprint.cancelAuthentication(); this.props.navigation.navigate("Login")}} 
                            style={{width:"49.75%",height:"24%",marginTop:"10%",marginLeft:"0.5%", backgroundColor:"#e0e0e0", borderBottomRightRadius:5,alignItems:"center", justifyContent:"center"}}>
                                <Text style={{fontFamily:"Raleway-Bold", color:"#03aede", fontSize:responsiveFontSize(1.7)}}>Masukkan Password</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                       {
                        this.state.isLoading ?
                            <View style={{ position: 'absolute', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%', zIndex: 2, backgroundColor: 'rgba(0,0,0,.5)' }}>
                                <ActivityIndicator color="#ee3968" size="large" />
                                <Text style={{ color: 'white', fontSize: 13, fontFamily: 'Raleway-Reguler' }}>Loading..</Text>
                            </View> : null
                    }
                 
            </Container>
        );
    }

  

      login = () => {
        this.setState({isLoading:true})
        this.setState({phase:""})
        if(this.state.buttonEnabled){
              this.setState({ isLoading:false });
        } else{
              this.setState({ isLoading:true });
        }
      
        var details = {
            'userName': this.state.email,
            'password': this.state.password,
            'datetime': this.state.datetime,
            'terminal': GLOBAL.TERMINAL,

        };

        var formBody = [];
        for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        fetch(GLOBAL.BASE_URL + "/api/sign-in", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        })
            .then(response => response.json())
            .then((data) => {
                this.setState({ isLoading:false });   
                console.log(data);
                    this.setState({isLoading:false})
                
                    if (data.resultCode != "0") {
                        this.setState({
                            error: data.resultDesc,
                            login: true
                        });
                    } else {
                        this.setState({ login: false });
                        AsyncStorage.setItem("idTmoney", data.user.idTmoney);
                        AsyncStorage.setItem("email", this.state.email);
                        AsyncStorage.setItem("idFusion", data.user.idFusion);
                        AsyncStorage.setItem("token", data.user.token);
                        AsyncStorage.setItem("custName", data.user.custName);
                        AsyncStorage.setItem("balance", data.user.balance);
                        var details = {
                            'terminal': GLOBAL.TERMINAL,
                            'idTmoney': data.user.idTmoney,
                            'idFusion': data.user.idFusion,
                            'token': data.user.token,
                            'apikey': GLOBAL.API_KEY
                        };

                        var formBody = [];
                        for (var property in details) {
                            var encodedKey = encodeURIComponent(property);
                            var encodedValue = encodeURIComponent(details[property]);
                            formBody.push(encodedKey + "=" + encodedValue);
                        }
                        formBody = formBody.join("&");

                        fetch(GLOBAL.BASE_URL + "/api/my-profile", {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
                            },
                            body: formBody
                        })
                            .then(response => response.json())
                            .then((data) => {
                                console.log(data);
                                console.log(details);
                                console.log(formBody);
                                if (data.resultCode != "0") {
                                    this.setState({
                                        error: data.resultDesc
                                    });
                                } else {
                                    AsyncStorage.setItem("userType", data.userType);
                                }

                            })
                        
                        //this.props.navigation.navigate("TabNavigator")
                    
                }
            })
    }
}